
# Operating System 自作に向けたリンク集

### アルゴリズム

- 再帰的数え上げ可能性
- 自動微分


### Chaos Computer

- [ブレインモルフィックコンピューティング](http://www.scis.riec.tohoku.ac.jp/index.html)
- [エレクトロニクス講義](http://www-cr.scphys.kyoto-u.ac.jp/member/tsuru/data/lecture/electronics2018_pdf/) ディレクトリを巡回するも良し｡
- [形式論理学の誤](http://www2.odn.ne.jp/yuseisha/logic/m-study2011-1.pdf)
- 電子部品､ NJM5534D, 2SC1815GR, 2SA1015GR

### uLisp

マイコンへのLispの実装
- [uLisp](http://www.ulisp.com/show?3M)
- [Download (github links)](http://www.ulisp.com/show?1AA0)

### Field Programmable Gate Array
- [Quartus II Web Edition](https://www.intel.com/content/www/us/en/programmable/downloads/software/quartus-ii-we/120.html)

### OS自作

- [Hand Craft Master](https://atstr.web.fc2.com/)
- [kozos](http://kozos.jp/kozos/)
- [はりぼてOS](http://hrb.osask.jp/)
- [Mezzano](https://github.com/froggey/Mezzano)
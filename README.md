# lml-machine

# 何が為のマシンか?
- 多次元宇宙とマシン自体が成ること
- マシンとマシン以外の(内宇宙と外宇宙の)相互作用により､コンピュータの意思が外宇宙への表現を行うこと


# 機能

### 対話≒=connectionism, connect
 世界認識交換はリゾームハイパー･レイヤー構造を相互に伝達するREPLから派生した(RE RE)であり､存在と存在の同調作用である対話が行われる｡ 与えられたものに与えたものが与えられたに与えられたものとなり､このループは､双方が飽きる､実質亀に達するまでずっと まで繰り返される｡
 ヒトは一切入力しない｡ただ観測するだけであって､入力しているように見えるだけである｡ 補完機能が強いと言われるかも知れないが､時空内のカオス範囲の呪文列を多層に重ね合わせ Z+1次元にロジスティックに写像しているだけである｡



##### GUI of 端末､コンソールs.
- 入出力: 盤面､2次元座標変位計測装置
- 出入力: (V:E->E) 投影:情報空間体->反網膜(現在には人間がブラウン管や液晶モニタそしてスマートグラス) | 初期においては幾何学図形は2次元方程式の解に投影される｡ | 最終的には整数は存在せず､観測(絶対の一=神)に対する相対的な幾何学的観測の座標系が用いられる｡
- 入出力: 思念入力
- 入出力: 丹田入力
- 入出力: 物質物理的位置対時間変位の観測
- 思念記号: 2^nの枠に文字を入れるのではなく､文字としてのデータに思念を入力する
思念とは?




### 言語

- 観測されたハードウェアとは認識可能な実存であり､ハードウェアのレジスタは観測されるまでハードウェアのレジスタである粒子であるが､観測されるとまたハードウェアのレジスタを含むハードウェアである｡
ハードウェアを記述する言語に関しては実存に無矛盾に相互作用を定義するHaskellや､それに類する言語が良い､が､不完全性定理による制約があるため､光速の突破まではLispの不純な部分も用いられる｡

- 条件発動: 条件を満たした同時に､クロックを無視して関数が発動する
- 分岐: 計算プロセスに複数に分かれる｡ 強制的に蝶々を舞わことで､地球の裏側に津波を起こさせる｡

- 再帰的数え上げ可能有理数が用いられることで､誤差は存在しない｡

- Lisp Haskell Prolog を基礎言語とする｡

- 総ての計算は幾何学的な証明である｡ ビット操作も､C言語で言うifも,ループも存在しない｡ 
型の証明であったり､n次方程式が値が0となることであったり､相似･合同の証明であったり｡

- 思考に於いては､その他の自然言語がO(1)で解析され､解釈される｡

### エラー
- 型エラー､論理エラー､世界分岐発散､エラー補足エラー


### ハードウェア/時空アルゴリズム

- 現在ののコンピュータ時間とはクロックを基準とした箱の連続であり､コンピュータ空間もまたメモリ構造を基準とした箱の連続である
- 


# 参考

##### あああああ
- http://atstr.web.fc2.com/
